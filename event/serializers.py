from rest_framework import serializers
from .models import Post, Session

class SessionSerializer(serializers.ModelSerializer):

    conference_name = serializers.CharField(source='post.title')
    date = serializers.DateField(source='post.date')
    class Meta:
        model= Session
        fields= ['conference_name','date','title','time','speaker','session_link']
        